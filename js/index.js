let parcileInitedSlides = [];
const particlesSettings = {
    "particles": {
        "number": {
            "value": 80,
            "density": {
                "enable": true,
                "value_area": 800
            }
        },
        "color": {
            "value": "#ffffff"
        },
        "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 5
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
        },
        "opacity": {
            "value": 0.5,
            "random": false,
            "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.1,
                "sync": false
            }
        },
        "size": {
            "value": 5,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
            }
        },
        "line_linked": {
            "enable": true,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
        },
        "move": {
            "enable": true,
            "speed": 6,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
            }
        }
    },
    "interactivity": {
        "detect_on": "canvas",
        "events": {
            "onhover": {
                "enable": true,
                "mode": "repulse"
            },
            "onclick": {
                "enable": true,
                "mode": "push"
            },
            "resize": true
        },
        "modes": {
            "grab": {
                "distance": 400,
                "line_linked": {
                    "opacity": 1
                }
            },
            "bubble": {
                "distance": 400,
                "size": 40,
                "duration": 2,
                "opacity": 8,
                "speed": 3
            },
            "repulse": {
                "distance": 200
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
        }
    },
    "retina_detect": true,
    "config_demo": {
        "hide_card": false,
        "background_color": "#b61924",
        "background_image": "",
        "background_position": "50% 50%",
        "background_repeat": "no-repeat",
        "background_size": "cover"
    }
}
const colorThief = new ColorThief();

const rgbToHex = (r, g, b) => '#' + [r, g, b].map(x => {
    const hex = x.toString(16)
    return hex.length === 1 ? '0' + hex : hex
}).join('');

const getSliderBackground = (index) => {
    let color = {};
    let particles = null;
    let img = $('.one-large-slide').eq(index).find('img');
    img = img[0];

    if (img.complete) {
        color = colorThief.getColor(img);
    } else {
        img.addEventListener('load', function () {
            color = colorThief.getColor(img);
        });
    }



    if (color) {
        $('.one-large-slide').eq(index).css('background-color', rgbToHex(color[0], color[1], color[2]))
        if (parcileInitedSlides.indexOf(index) == -1) {

            parcileInitedSlides.push(index);
            // particles = new particlesJS(`tns1-item${index-1}`, particlesSettings);
        }
    }
}

let getMainSlider = function(){
    if (! $(document).find('.slider-in').length)
        return false;


    let slider = tns({
        container: '.slider-in',
        items: 1,
        slideBy: 1,
        autoplay: true,
        nav: false,
        controlsText: ["", ""],
        autoplayButton: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        onInit: function () {
            try {
                getSliderBackground(1);
            } catch (e) {}
        }
    });

    slider.events.on('transitionStart', function (info, eventName) {

    })

    slider.events.on('transitionEnd', function (info, eventName) {
        try {
            getSliderBackground(info.index);
        } catch (e) {}
    });

    return slider;
}

let getMiniSlider = function(){
    if (! $(document).find('.mini-slider').length)
        return false;

    let mini_slider = tns({
        container: '.mini-slider',
        items: 2,
        slideBy: 1,
        autoplay: false,
        nav: false,
        arrowKeys: false,
        nav: false,
        controls: false,
        gutter: 40,
        controlsText: ["", ""],
        responsive: {
            320: {
                items: 1
            },
            767: {
                items: 2
            },
        }
    });

    return mini_slider;
};

let getEventsSlider = function(){
    if (! $(document).find('.upcoming-events__list').length)
        return false;

    if($(window).width() > 767 && $(document).find('.upcoming-events__event').length <= 4){
        $('.upcoming-events__list').addClass('no-slider')
        return false
    }
        

    let events_slider = tns({
        container: '.upcoming-events__list',
        items: 4,
        slideBy: 1,
        autoplay: false,
        gutter: 30,
        nav: false,
        controlsText: ["", ""],
        responsive: {
            320: {
                items: 1
            },

            767: {
                items: 3,
                // gutter: 25,
            },

            992: {
                items: 4
            },
        }
    });

    return events_slider;
};

let getBioSlider = function(params){
    if (! params.container || !params){
        console.warn('params.container is required');
        return false;
    }

    if (! $(document).find(params.container).length)
        return false;

    let bio_slider = tns({
        container: params.container,
        items: params.items || 1,
        slideBy: 1,
        autoplay: params.autoplay || false,
        gutter: params.gutter || 0,
        nav: params.nav || false,
        controlsText: ["", ""],
        navPosition: "bottom",
        controls: params.controls || true,
    });

    return bio_slider;
}

let getCardsSlider = function(params){
    if (! params.container || !params){
        console.warn('params.container is required');
        return false;
    }

    let defaultResponciveParams = {
        320: {
          items: 1
        },

        767: {
          items: 2
        },

        992: {
          items: 3
        },
    };

    if(! $(params.container).length )
        return false

    let cards_slider = tns({
        container: params.container,
        items: params.items || 3,
        slideBy: 1,
        loop: $(params.container).find('.slide-card').length > 3,
        autoplay: params.autoplay || true,
        autoplayTimeout: params.autoplayTimeout || 2000,
        gutter: params.gutter || 30,
        nav: params.nav || true,
        controlsText: ["", ""],
        navPosition: "bottom",
        controls: params.controls || true,
        responsive: params.responsive || defaultResponciveParams,
    });

    return cards_slider;
};

function openBioSlider(){
    $('.bio-slider').fadeIn();
};

function initMap() {
    if(!$('#map').length)
        return false;

    let map, marker, latlng;
    latlng = {lat: 40.757107, lng: -73.989602};
    map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 12
    });

    marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: ''
    });
    return {map, marker};
}

function productDetailZoom(){
    if($(".product-picture").length)
        $(".product-picture").elevateZoom();
}

function productDetailSlider() {
    if(! $('.slider-large').length || !$('.slider-small').length)
        return false;

    $('.slider-large').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-small'
      });
      $('.slider-large').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var img = $(slick.$slides[nextSlide]);
        console.log('img', img)
        $('.zoomWindowContainer,.zoomContainer').remove();
        img.elevateZoom();
     }); 
      $('.slider-small').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-large',
        dots: false,
        arrows: $('.slider-preview').length > 3,
        centerMode: false,
        focusOnSelect: true,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
      });
}

function drawTableScrollFade(){
    let $agendaBlock = $('.table-agenda');
    let $agendaBlockTableCotainer = $agendaBlock.find('.table-container');
    let $agendaBlockTable = $agendaBlock.find('table');
    let fade = false;

    if($agendaBlockTable.width() > $agendaBlockTableCotainer.width()){
        if(!window.hasScrollFadeBlock){
            $agendaBlock.append('<div class="scrollFade"></div>');
            window.hasScrollFadeBlock = true;
        }        
    } else {
        $(document).find('.scrollFade').remove();
        window.hasScrollFadeBlock = false;
    }
}

function openPopupForm(){
    $('.popup-form-container').css('display', 'flex')
}

$(window).on('resize', function(){
    drawTableScrollFade();
})

$(document).ready(function () {
    getMainSlider();
    // getMiniSlider();
    getEventsSlider();
    drawTableScrollFade();

    $('.mobile_burger').on('click', function() {
        $('.header').toggleClass('page-menu-opened');
        $('.nav').toggleClass('menu-opened');
        $(document).find('.submenu-opened').removeClass('submenu-opened');
    })

    //CLASS FOR SLIDER MUST BE UNIQUE (OR USE ID)!
    getCardsSlider({
        container: '.cards-slider-in__speakers',
    });
    getCardsSlider({
        container: '.cards-slider-in__platinum',
    });
    getCardsSlider({
        container: '.cards-slider-in__gold',
        responsive: {
            320: {
              items: 2,
              gutter: 10,
            },
    
            767: {
              items: 3,
              gutter: 10,
            },
    
            1400: {
              items: 4
            },
        }
    });
    getCardsSlider({
        container: '.cards-slider-in__silver',
        responsive: {
            320: {
              items: 2,
              gutter: 10,
            },
    
            767: {
              items: 3,
              gutter: 10,
            },
    
            1400: {
              items: 4
            },
        }
    });
    getCardsSlider({
        container: '.cards-slider-in__testimonials',
        items: 1,
        responsive: {
            320: {
                items: 1
            },
        }
    });

    const bioSlider = getBioSlider({
        container: '#speakers-bio'
    });

    $('.js-view-bio').on('click', function(){
        openBioSlider();
        bioSlider.goTo($(this).data('slide'));
    });

    $('.bio-slider .close').on('click', function(){
        $('.bio-slider').fadeOut();
    });

    $('.nav a').on('click', function(e){
        if($(this).siblings('.submenu-outer').length){
            if($(window).width() < 768){
                //for mobile
                e.preventDefault();
                $(document).find('.submenu-opened').not($(this).parent('li')).removeClass('submenu-opened');
                $(this).parent('li').toggleClass('submenu-opened');
            }
            
        }
    })
    
    productDetailSlider();
    productDetailZoom();
    if($(".particles-js").length)
        particlesJS('particles-js', particlesSettings);
    initMap();
})